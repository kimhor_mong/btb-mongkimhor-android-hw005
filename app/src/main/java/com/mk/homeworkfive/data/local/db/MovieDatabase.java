package com.mk.homeworkfive.data.local.db;


import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.mk.homeworkfive.data.local.db.dao.CategoryDao;
import com.mk.homeworkfive.data.local.db.dao.MovieDao;
import com.mk.homeworkfive.data.model.Category;
import com.mk.homeworkfive.data.model.Movie;

@Database(entities = {Movie.class, Category.class}, exportSchema = false, version = 2)
public abstract class MovieDatabase extends RoomDatabase {
    private static final String DB_NAME = "movie_db";
    private static MovieDatabase movieDatabaseInstance;

    public static synchronized MovieDatabase getInstance(Context context){
        if(movieDatabaseInstance == null){
            movieDatabaseInstance = Room.databaseBuilder(context, MovieDatabase.class, DB_NAME)
                    .fallbackToDestructiveMigration()
                    .allowMainThreadQueries()
                    .build();
        }
        return movieDatabaseInstance;
    }

    public abstract MovieDao movieDao();
    public abstract CategoryDao categoryDao();
}
