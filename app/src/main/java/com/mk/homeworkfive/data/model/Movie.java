package com.mk.homeworkfive.data.model;

import android.security.AppUriAuthenticationPolicy;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(tableName = "movie", 
        foreignKeys = @ForeignKey(entity = Category.class, parentColumns = "id", childColumns = "category_id"))
public class Movie {
    @PrimaryKey(autoGenerate = true)
    private int id;
    @ColumnInfo(name = "name")
    private String name;
    @ColumnInfo(name = "image")
    private int image;
    @ColumnInfo(name = "category_id")
    private int categoryId;

    @Ignore
    public Movie(){}

    @Ignore
    public Movie(String name, int image, int categoryId){
        this.name = name;
        this.image = image;
        this.categoryId = categoryId;
    }

    @Ignore
    public Movie(int id, String name, int image){
        this.id = id;
        this.name = name;
        this.image = image;

    }


    public Movie(int id, String name, int image, int categoryId) {
        this.id = id;
        this.image = image;
        this.name = name;
        this.categoryId = categoryId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    @Override
    public String toString() {
        return "{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", image=" + image +
                ", categoryId=" + categoryId +
                '}';
    }
}
