package com.mk.homeworkfive.ui.main.category;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mk.homeworkfive.R;
import com.mk.homeworkfive.data.model.Movie;

import java.util.List;

public class TrendingMovieAdapter extends RecyclerView.Adapter<TrendingMovieAdapter.TrendingMovieViewHolder> {
    private List<Movie> mTrendingMovies;
    private Context mContext;

    public TrendingMovieAdapter(Context context, List<Movie> trendingMovies) {
        this.mContext = context;
        this.mTrendingMovies = trendingMovies;
    }

    @NonNull
    @Override
    public TrendingMovieViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_trending_movie, parent, false);
        return new TrendingMovieViewHolder(view);
    }

    @Override
    public void onBindViewHolder(TrendingMovieViewHolder holder, int position) {
        holder.imageViewTrendingMovie.setImageResource(mTrendingMovies.get(position).getImage());
    }

    @Override
    public int getItemCount() {
        return mTrendingMovies.size();
    }

    public static class TrendingMovieViewHolder extends RecyclerView.ViewHolder{
        public ImageView imageViewTrendingMovie;

        public TrendingMovieViewHolder(View itemView) {
            super(itemView);
            imageViewTrendingMovie = itemView.findViewById(R.id.image_view_trending_movie);
        }
    }

}