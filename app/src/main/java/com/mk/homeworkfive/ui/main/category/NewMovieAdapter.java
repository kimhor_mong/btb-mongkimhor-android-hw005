package com.mk.homeworkfive.ui.main.category;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mk.homeworkfive.R;
import com.mk.homeworkfive.data.model.Movie;

import java.util.List;

public class NewMovieAdapter extends RecyclerView.Adapter<NewMovieAdapter.NewMovieViewHolder> {
    private Context mContext;
    private List<Movie> mNewMovies;

    public NewMovieAdapter(Context context, List<Movie> newMovies){
        this.mContext = context;
        this.mNewMovies = newMovies;
    }

    @NonNull
    @Override
    public NewMovieViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext)
                .inflate(R.layout.item_new_movie,parent, false);
        return new NewMovieViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NewMovieViewHolder holder, int position) {
        Movie newMovie = mNewMovies.get(position);
        holder.imageViewNewMovie.setImageResource(newMovie.getImage());
        holder.textViewNewMovieName.setText(newMovie.getName());
    }

    @Override
    public int getItemCount() {
        return mNewMovies.size();
    }

    public static class NewMovieViewHolder extends RecyclerView.ViewHolder {
        ImageView imageViewNewMovie;
        TextView textViewNewMovieName;
        public NewMovieViewHolder(@NonNull View itemView) {
            super(itemView);
            imageViewNewMovie = itemView.findViewById(R.id.image_view_new_movie);
            textViewNewMovieName = itemView.findViewById(R.id.text_view_new_movie_name);
        }
    }
}
