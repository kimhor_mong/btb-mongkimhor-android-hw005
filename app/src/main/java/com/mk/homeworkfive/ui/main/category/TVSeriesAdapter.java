package com.mk.homeworkfive.ui.main.category;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mk.homeworkfive.R;
import com.mk.homeworkfive.data.model.Movie;


import java.util.List;

public class TVSeriesAdapter extends RecyclerView.Adapter<TVSeriesAdapter.TVSeriesViewHolder> {
    private Context mContext;
    private List<Movie> mTvSeries;

    public TVSeriesAdapter(Context context, List<Movie> tvSeries){
        this.mContext = context;
        this.mTvSeries = tvSeries;
    }

    @NonNull
    @Override
    public TVSeriesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext)
                .inflate(R.layout.item_tv_series,parent, false);
        return new TVSeriesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TVSeriesViewHolder holder, int position) {
        Movie newMovie = mTvSeries.get(position);
        holder.imageViewTvSeries.setImageResource(newMovie.getImage());
        holder.textViewTvSeriesName.setText(newMovie.getName());
    }

    @Override
    public int getItemCount() {
        return mTvSeries.size();
    }

    public static class TVSeriesViewHolder extends RecyclerView.ViewHolder {
        ImageView imageViewTvSeries;
        TextView textViewTvSeriesName;
        public TVSeriesViewHolder(@NonNull View itemView) {
            super(itemView);
            imageViewTvSeries = itemView.findViewById(R.id.image_view_tv_series);
            textViewTvSeriesName = itemView.findViewById(R.id.text_view_tv_series_name);
        }
    }
}
