package com.mk.homeworkfive.ui.main.category;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.mk.homeworkfive.R;
import com.mk.homeworkfive.data.model.Movie;

import java.util.List;

public class MayLikeMovieAdapter extends RecyclerView.Adapter<MayLikeMovieAdapter.MayLikeMovieViewHolder> {
    Context mContext;
    List<Movie> mMayLikeMovies;

    public MayLikeMovieAdapter(Context context, List<Movie> mMayLikeMovies) {
        this.mMayLikeMovies = mMayLikeMovies;
        this.mContext = context;
    }

    @NonNull
    @Override
    public MayLikeMovieViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.item_may_like_movie, parent, false);
        return new MayLikeMovieViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MayLikeMovieViewHolder holder, int position) {
        Movie mayLikeMovie = mMayLikeMovies.get(position);
        holder.imageViewMayLikeMovie.setImageResource(mayLikeMovie.getImage());
    }

    @Override
    public int getItemCount() {
        return mMayLikeMovies.size();
    }

    public static class MayLikeMovieViewHolder extends RecyclerView.ViewHolder {
        public ImageView imageViewMayLikeMovie;
        public MayLikeMovieViewHolder(@NonNull View itemView) {
            super(itemView);
            imageViewMayLikeMovie = itemView.findViewById(R.id.image_view_may_like_movie);
        }
    }
}
