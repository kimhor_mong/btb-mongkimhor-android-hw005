package com.mk.homeworkfive.ui.main;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.mk.homeworkfive.R;
import com.mk.homeworkfive.data.local.db.MovieDatabase;
import com.mk.homeworkfive.data.model.Category;
import com.mk.homeworkfive.data.model.Movie;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

//        insertMovieCategories();
//        insertMovies();


        RecyclerView recyclerViewMovieCategory = findViewById(R.id.recycler_view_movie_category);

        MovieCategoryAdapter movieCategoryAdapter = new MovieCategoryAdapter(getMovieCategories(), MainActivity.this);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);

        recyclerViewMovieCategory.setLayoutManager(layoutManager);
        recyclerViewMovieCategory.setAdapter(movieCategoryAdapter);

        movieCategoryAdapter.notifyDataSetChanged();
    }

    private List<Category> getMovieCategories() {
        MovieDatabase movieDatabase = MovieDatabase.getInstance(this);
        return movieDatabase.categoryDao().getCategories();
    }

    private void insertMovieCategories() {
        MovieDatabase movieDatabase = MovieDatabase.getInstance(MainActivity.this);
        movieDatabase.categoryDao().insertCategory(new Category("Trending"));
        movieDatabase.categoryDao().insertCategory(new Category("New Movies"));
        movieDatabase.categoryDao().insertCategory(new Category("You May Like"));
        movieDatabase.categoryDao().insertCategory(new Category("TV Series"));
    }

    private void insertMovies() {
        MovieDatabase movieDatabase = MovieDatabase.getInstance(MainActivity.this);

        movieDatabase.movieDao().insertMovie(new Movie("Movie Name", R.drawable.finch_large_cover_image, 1));
        movieDatabase.movieDao().insertMovie(new Movie("Movie Name", R.drawable.telolw_large_cover_image, 1));
        movieDatabase.movieDao().insertMovie(new Movie("Movie Name", R.drawable.seraphim_falls_large_cover_image, 1));
        movieDatabase.movieDao().insertMovie(new Movie("Movie Name", R.drawable.the_harder_they_fall_large_cover_image, 1));

        movieDatabase.movieDao().insertMovie(new Movie("Movie Name", R.drawable.dangerous_medium_cover, 2));
        movieDatabase.movieDao().insertMovie(new Movie("Movie Name", R.drawable.kp2_medium_cover, 2));
        movieDatabase.movieDao().insertMovie(new Movie("Movie Name", R.drawable.shang_chi_medium_cover, 2));
        movieDatabase.movieDao().insertMovie(new Movie("Movie Name", R.drawable.the_courier_medium_cover, 2));

        movieDatabase.movieDao().insertMovie(new Movie("Movie Name", R.drawable.bestofoscar1, 3));
        movieDatabase.movieDao().insertMovie(new Movie("Movie Name", R.drawable.bestofoscar2, 3));
        movieDatabase.movieDao().insertMovie(new Movie("Movie Name", R.drawable.bestofoscar3, 3));
        movieDatabase.movieDao().insertMovie(new Movie("Movie Name", R.drawable.bestofoscar4, 3));

        movieDatabase.movieDao().insertMovie(new Movie("Movie Name", R.drawable.db_medium_cover, 4));
        movieDatabase.movieDao().insertMovie(new Movie("Movie Name", R.drawable.tpoth_medium_cover_image, 4));
        movieDatabase.movieDao().insertMovie(new Movie("Movie Name", R.drawable.ntod_medium_cover, 4));
        movieDatabase.movieDao().insertMovie(new Movie("Movie Name", R.drawable.ej_medium_cover, 4));

    }

}