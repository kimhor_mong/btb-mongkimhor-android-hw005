package com.mk.homeworkfive.ui.main;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mk.homeworkfive.R;
import com.mk.homeworkfive.data.local.db.MovieDatabase;
import com.mk.homeworkfive.data.local.db.dao.MovieDao;
import com.mk.homeworkfive.data.model.Category;
import com.mk.homeworkfive.data.model.Movie;
import com.mk.homeworkfive.ui.main.category.MayLikeMovieAdapter;
import com.mk.homeworkfive.ui.main.category.NewMovieAdapter;
import com.mk.homeworkfive.ui.main.category.TVSeriesAdapter;
import com.mk.homeworkfive.ui.main.category.TrendingMovieAdapter;

import java.util.List;

public class MovieCategoryAdapter extends RecyclerView.Adapter<MovieCategoryAdapter.MovieCategoryViewHolder> {
    public Context context;
    private List<Category> mMovieCategories;
    private MovieDao mMovieDao;


    public MovieCategoryAdapter(List<Category> movieCategories, Context context) {
        this.mMovieCategories = movieCategories;
        this.context = context;

        mMovieDao = MovieDatabase.getInstance(context).movieDao();
    }

    @NonNull
    @Override
    public MovieCategoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_movie_category, parent, false);

        return new MovieCategoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MovieCategoryViewHolder holder, int position) {
        Category currentCategory = mMovieCategories.get(position);

        holder.textViewMovieCategory.setText(currentCategory.getName());

        List<Movie> moviesOfCategory = mMovieDao.getMoviesByCategoryId(currentCategory.getId());

        switch (currentCategory.getName()) {
            case "Trending":
                TrendingMovieAdapter trendingMovieAdapter = new TrendingMovieAdapter(context, moviesOfCategory);
                holder.recyclerMovieCategoryDetail.setAdapter(trendingMovieAdapter);
                break;
            case "New Movies":
                NewMovieAdapter newMovieAdapter = new NewMovieAdapter(context, moviesOfCategory);
                holder.recyclerMovieCategoryDetail.setAdapter(newMovieAdapter);

                break;
            case "You May Like":
                MayLikeMovieAdapter mayLikeMovieAdapter = new MayLikeMovieAdapter(context, moviesOfCategory);
                holder.recyclerMovieCategoryDetail.setAdapter(mayLikeMovieAdapter);
                break;
            case "TV Series":

                TVSeriesAdapter tvSeriesAdapter = new TVSeriesAdapter(context, moviesOfCategory);
                holder.recyclerMovieCategoryDetail.setAdapter(tvSeriesAdapter);
                break;
        }

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false);
        holder.recyclerMovieCategoryDetail.setLayoutManager(layoutManager);
    }

    @Override
    public int getItemCount() {
        return mMovieCategories.size();
    }

    public static class MovieCategoryViewHolder extends RecyclerView.ViewHolder {
        public TextView textViewMovieCategory;
        public RecyclerView recyclerMovieCategoryDetail;

        public MovieCategoryViewHolder(View itemView) {
            super(itemView);
            textViewMovieCategory = itemView.findViewById(R.id.label_movie_category);
            recyclerMovieCategoryDetail = itemView.findViewById(R.id.recycler_view_movie_category_detail);
        }
    }
}
